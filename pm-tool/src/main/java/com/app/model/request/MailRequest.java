package com.app.model.request;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MailRequest {

    private String email;
    private String subject;
    private String description;
    private LocalDateTime createdTime;
}
