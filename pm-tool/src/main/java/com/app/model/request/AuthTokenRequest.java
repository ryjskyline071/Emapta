package com.app.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AuthTokenRequest {

    @NotBlank(message = "Username is required")
    private String username;

    @NotBlank(message = "Password is required")
    private String password;

}
