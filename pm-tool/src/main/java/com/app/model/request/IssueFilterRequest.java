package com.app.model.request;

import com.app.model.enums.IssueState;
import com.app.model.enums.IssueType;
import lombok.Data;

@Data
public class IssueFilterRequest {

    private Integer projectId;
    private String issueState;
    private String issueType;
}
