package com.app.model.reponse;

import lombok.Data;

@Data
public class AuthTokenResponse {
    private final String jwtToken;
}
