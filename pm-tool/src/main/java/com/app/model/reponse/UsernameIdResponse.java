package com.app.model.reponse;

import lombok.Data;

@Data
public class UsernameIdResponse {
    private String username;
    private Integer userId;
}




