package com.app.util;

import com.app.exception.DataNotFoundException;
import com.app.model.request.ProjectRequest;

public class Projectvalidator {

    public static void validateProject(ProjectRequest projectRequest) {

        if(projectRequest == null || projectRequest.getProjectName() == null){
            throw new DataNotFoundException("Project data required");
        }
        if(projectRequest.getProjectName() == null || projectRequest.getProjectName().equals("")){
            throw new DataNotFoundException("Project name is required");
        }
        if(projectRequest.getDescription() == null){
            throw new DataNotFoundException("Project description is required");
        }
        if(projectRequest.getCreatedUser() == null || projectRequest.getCreatedUser().equals("")){
            throw new DataNotFoundException("Project created user is required");
        }
    }
}
