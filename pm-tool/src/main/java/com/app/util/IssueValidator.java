package com.app.util;

import com.app.exception.BadRequestException;
import com.app.model.request.IssueRequest;

public class IssueValidator {
    public static void validateRequest(IssueRequest issueRequest) {

        if(issueRequest == null){
            throw new BadRequestException("Request data is required");
        }
        if(issueRequest.getProjectId() == null){
            throw new BadRequestException("Project ID is required");
        }
        if(issueRequest.getIssueType() == null || issueRequest.getIssueType().getCode().equals("")){
            throw new BadRequestException("Issue type is required");
        }
        if(issueRequest.getIssueState() == null || issueRequest.getIssueState().getCode().equals("")){
            throw new BadRequestException("Issue state is required");
        }
        if(issueRequest.getIssueDescription() == null || issueRequest.getIssueDescription().equals("")){
            throw new BadRequestException("Issue description is required");
        }
        if(issueRequest.getCreatedUser() == null || issueRequest.getCreatedUser().equals("")){
            throw new BadRequestException("Issue created user is required");
        }
    }
}
