package com.app.util;

import java.time.LocalDateTime;

public class SecurityConst {

    public static final String SECRET = "q3t6w9z$C&F)J@NcQfTjWnZr4u7x!A%D*G-KaPdSgUkXp2s5v8y/B?E(H+MbQeTh";

    public static final Long VALIDITY_TIME = 1000L*60*30;

}
