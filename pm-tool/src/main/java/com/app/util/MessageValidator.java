package com.app.util;

import com.app.exception.BadRequestException;
import com.app.model.request.MessageRequest;
import lombok.Data;

@Data
public class MessageValidator {

    public static void validateRequest(MessageRequest messageRequest) {

        if(messageRequest == null){
            throw new BadRequestException("Message request is required");
        }
        if(messageRequest.getEmailMessage() == null || messageRequest.getEmailMessage().equals("")){
            throw new BadRequestException("Email message request is required");
        }
        if(messageRequest.getMessage() == null || messageRequest.getMessage() .equals("")){
            throw new BadRequestException("Message details are required");
        }
        if(messageRequest.getNotification() == null || messageRequest.getNotification().equals("")){
            throw new BadRequestException("Notification is required");
        }

    }
}
