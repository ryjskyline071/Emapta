package com.app.util;

import com.app.exception.BadRequestException;
import com.app.model.common.CommonResponse;
import com.app.model.request.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;

public class UserValidator {

    @Autowired
    private CommonResponse commonResponse;

    public static void validateRequest(UserRequest userRequest) {

        if (userRequest == null) {
             throw new BadRequestException("Request body is null");
        }
        if (userRequest.getUsername() == null || userRequest.getUsername().equals("")) {
            throw new BadRequestException("Username is null or empty");
        }
        if(userRequest.getEmail() == null || userRequest.getEmail().equals("")){
            throw new BadRequestException("User email os null or empty");
        }
    }
}
