package com.app.util;

import com.app.exception.BadRequestException;
import com.app.model.request.MailRequest;

public class MailValidator {

    public static void validateMail(MailRequest mailRequest) {

        if(mailRequest == null){
            throw new BadRequestException("Mail request is required");
        }
        if(mailRequest.getEmail() == null || mailRequest.getEmail().equals("")){
            throw new BadRequestException("Mail id is required");
        }
        if(mailRequest.getSubject() == null || mailRequest.getSubject().equals("")){
            throw new BadRequestException("Mail subject is required");
        }
        if(mailRequest.getDescription() == null || mailRequest.getDescription().equals("")){
            throw new BadRequestException("Mail description is required");
        }
    }
}
