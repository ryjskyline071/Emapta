package com.app.util;

public enum CommonMessage {
    SUCCESS(0, "Success"),
    FAIL(1, "Fail"),
    CREATED(3, "Created"),
    UPDATED(4, "Updated"),
    BAD_REQUEST(5, "Bad Request"),
    NOT_FOUND(7, "Not Found"),
    BAD_GATEWAY(9, "Bad Gateway"),
    AUTH_HEADER(11,"Authorization");

    private final int value;
    private String message;

    CommonMessage(int value, String message) {
        this.value = value;
        this.message = message;
    }

    public int value() {
        return value;
    }

    public String message() {
        return message;
    }
}
