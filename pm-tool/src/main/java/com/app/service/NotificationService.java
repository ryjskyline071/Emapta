package com.app.service;

import com.app.model.request.MessageRequest;
import com.app.util.MessageValidator;
import com.google.firebase.messaging.FirebaseMessaging;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class NotificationService {

    public String sendMessage(MessageRequest req, String token) throws Exception {

        // validate the request
        MessageValidator.validateRequest(req);

        com.google.firebase.messaging.Message message = com.google.firebase.messaging.Message.builder()
                .putData("forEmail", req.getEmailMessage())
                .putData("message", req.getMessage())
                .putData("timestamp", LocalDateTime.now().toString())
                .putData("notificationFor", req.getNotification())
                .setToken(token)
                .build();
        return FirebaseMessaging.getInstance().send(message);
    }
}
