package com.app.service;

import com.app.model.request.MailRequest;
import com.app.util.MailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailService {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(MailRequest mailRequest) {

        // validate mail request
        MailValidator.validateMail(mailRequest);

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("raviyasas@live.com", mailRequest.getEmail());
        msg.setSubject(mailRequest.getSubject());
        msg.setText(mailRequest.getDescription());

        javaMailSender.send(msg);
    }
}
