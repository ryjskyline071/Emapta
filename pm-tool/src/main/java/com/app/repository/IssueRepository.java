package com.app.repository;

import com.app.model.entity.Issue;
import com.app.model.enums.IssueState;
import com.app.model.enums.IssueType;
import com.app.model.request.IssueFilterRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Integer> {

    List<Issue> findAllByOrderByCreatedDateAsc();

    @Query(value="select * from issue a where a.project_id = :projectId", nativeQuery=true)
    List<Issue> findAllIssuesByProject(Integer projectId);

    @Query(value = "select * from issue where issue_project_id=?1 and issue_state=?2 and issue_type=?3", nativeQuery = true)
    List<Issue> findByIssueStateAndIssueType(Integer projectId, String issueState, String issueType);
}
