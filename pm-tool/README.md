Empowerteams 
============

Implemented features
--------------------

1. APIs for 
   * Create a user
   * Get users
   * Create a project
   * Get projects
   * Get a project
   * Create an issue
   * Get an issue
   * Get all issues
   * Update an issue
   * login
2. Open API specification
3. Spring Security and JWT token based authentication
4. Firebase notifications
5. Email notifications

IMPORTANT
---------
Below features are already commented out for dev testing.
   * Firebase notification
   * Email notification
   * permitAll() for all APIs in Security configuration