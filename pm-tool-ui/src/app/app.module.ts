import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { HomeComponent } from './component/home/home.component';
import { ProjectComponent } from './component/project/view-project/project.component';
import { IssueComponent } from './component/issue/view-issue/issue.component';
import { IndexComponent } from './component/index/index.component';
import { ProjectIssuesComponent } from './component/project/project-issues/project-issues.component';
import { AddProjectComponent } from './component/project/add-project/add-project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddIssueComponent } from './component/issue/add-issue/add-issue.component';
import { UpdateIssueComponent } from './component/issue/update-issue/update-issue.component';
import { ViewUserComponent } from './component/user/view-user/view-user.component';
import { AddUserComponent } from './component/user/add-user/add-user.component';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './component/user/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProjectComponent,
    IssueComponent,
    IndexComponent,
    ProjectIssuesComponent,
    AddProjectComponent,
    AddIssueComponent,
    UpdateIssueComponent,
    ViewUserComponent,
    AddUserComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
