interface GetBaseResponse{
    httpHeaders: {},
    httpStatusCode: number,
    apiStatusCode: number,
    message: string,
    data: Object[]
    additionalParams: {}
}
    

    