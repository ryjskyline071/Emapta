import { Issue } from "./issue";

export class Project {

    projectId: number | undefined;
    projectName: string | undefined;
    description: string | undefined;
    createdTime: string | undefined;
    createdUser: string | undefined;
    issues: Issue[] | undefined;
}
