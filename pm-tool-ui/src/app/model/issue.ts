import { ChangeLog } from "./change-log";
import { User } from "./user";

export class Issue {

    issue_id: number | undefined;
    issue_description: string | undefined;
    created_user: string | undefined;
    created_date: string | undefined;
    type: string | undefined;
    current_state: string | undefined;
    change_log: ChangeLog[] | undefined;
    //users: User[] | undefined;
    project_id: number| undefined;
    user_id: number | undefined;
}