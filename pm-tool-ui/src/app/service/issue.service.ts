import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Issue } from '../model/issue';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  constructor(private httpClient : HttpClient) { }

  issuesUrl = "http://localhost:8090/api/v1/issues";

  message : string |undefined;

  getIssues(): Observable<Issue[]>{
    return this.httpClient.get<getIssueData>(this.issuesUrl).pipe(
      map(response => response.data)
    );
  }

  saveIssue(issue: any){
    return this.httpClient.post(this.issuesUrl, issue);
  }

  getIssue(id: number): Observable<Issue>{
    return this.httpClient.get<getIssue>(this.issuesUrl+"/"+id).pipe(
      map(response => response.data)
    );
  }

  updateIssue(id: number, issue: Issue){
    this.httpClient.put<any>(this.issuesUrl+"/"+id, issue).subscribe({
      next: data => {
        this.message = data.message;
    },
    error: error => {
        console.error('There was an error!', error);
    }
    });
  }

}

interface getIssueData{
  data: Issue[]
}

interface getIssue{
  data: Issue;
}
