import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Issue } from '../model/issue';
import { Project } from '../model/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private httpClient : HttpClient) { }

  baseUrl = "http://localhost:8090/api/v1/";

  getProjects(): Observable<Project[]>{
    return this.httpClient.get<getProjectData>(this.baseUrl+"projects").pipe(
      map(response => response.data)
    );
  }

  getProjectIssues(id: number): Observable<Issue[]>{
    return this.httpClient.get<getProjectIssueData>(this.baseUrl+"issues/project/"+id).pipe(
      map(response => response.data)
    );
  }

  saveProject(project: any){
    return this.httpClient.post(this.baseUrl+"projects", project);
  }

}

interface getProjectData{
  data: Project[]
}

interface getProjectIssueData{
  data: Issue[]
}