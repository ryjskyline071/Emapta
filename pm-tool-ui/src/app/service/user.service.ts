import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient : HttpClient) { }

  baseUrl = "http://localhost:8090/api/v1/";

  getUsers(): Observable<User[]>{
    return this.httpClient.get<getUserData>(this.baseUrl+"users").pipe(
      map(response => response.data)
    );
  }

  getUsernames():Observable<string[]>{
    return this.httpClient.get<getUsernames>(this.baseUrl+"users/list").pipe(
      map(response => response.data)
    );
  }

  saveUser(user: any){
    return this.httpClient.post(this.baseUrl+"users/signup", user);
  }

}

interface getUserData{
  data: User[]
}

interface getUsernames{
  data: string[]
}