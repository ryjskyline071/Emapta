import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: []
})
export class ViewUserComponent implements OnInit {

  users: User[] = []

  constructor( private userService: UserService) { }

  ngOnInit(): void {
    this.listProjects();
  }

  listProjects(){
    this.userService.getUsers().subscribe(
      data => {
        this.users = data;
      }
    )
  }


}
