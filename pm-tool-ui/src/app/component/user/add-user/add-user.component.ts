import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: []
})
export class AddUserComponent implements OnInit {

  user: User = {
    user_id: undefined,
    username: undefined,
    password: undefined,
    repassword: undefined,
    email: undefined
  };

  constructor(private userService: UserService) {
  
  }

  ngOnInit(): void {
  }

  saveUser(){
    const project ={
      username : this.user.username,
      password : this.user.password,
      email : this.user.email,
    }

    this.userService.saveUser(this.user).subscribe(
      response => {
        alert("New user has been created");
        window.location.href = './users';
      },error => {
        console.log(error);
        alert("An error occured while saving the user");
      }
    )
  }

}
