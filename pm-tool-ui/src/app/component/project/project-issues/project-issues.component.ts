import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Issue } from 'src/app/model/issue';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-project-issues',
  templateUrl: './project-issues.component.html',
  styleUrls: []
})
export class ProjectIssuesComponent implements OnInit {

  id: number;
  issues: Issue[]  = [];
  
  constructor(private route: ActivatedRoute, private projectService: ProjectService) {
    //get id as a number
    this.id = +this.route.snapshot.params['projectId'];
  }

  ngOnInit(): void {
    this.getProjectIssues(this.id);
  }


  getProjectIssues(id: number){
    this.projectService.getProjectIssues(id).subscribe(
      data => {
        this.issues = data;
      }
    )
  }

}
