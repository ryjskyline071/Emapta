import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/model/project';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: []
})
export class AddProjectComponent implements OnInit {

  project: Project = {
    projectId: undefined,
    projectName: undefined,
    description: undefined,
    createdTime: undefined,
    createdUser: undefined,
    issues: undefined
  };

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {

  }

  saveProject(){
    const project ={
      projectName : this.project.projectName,
      description : this.project.description,
      createdUser : this.project.createdUser,
    }

    this.projectService.saveProject(project).subscribe(
      response => {
        alert("Project has been created");
        window.location.href = './projects';
      },error => {
        console.log(error);
        alert("An error occured while saving the project");
      }
    )
  }

}
