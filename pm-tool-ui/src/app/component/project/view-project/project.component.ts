import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/model/project';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: []
})
export class ProjectComponent implements OnInit {

  projects: Project[] = []

  constructor( private projectService: ProjectService) { }

  ngOnInit(): void {
    this.listProjects();
  }

  listProjects(){
    this.projectService.getProjects().subscribe(
      data => {
        this.projects = data;
      }
    )
  }

}
