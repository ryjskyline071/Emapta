import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Issue } from 'src/app/model/issue';
import { IssueService } from 'src/app/service/issue.service';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: []
})
export class IssueComponent implements OnInit {

  issues: Issue[]  = [];
  
  constructor(private route: ActivatedRoute, private issueService: IssueService) {
  }

  ngOnInit(): void {
    this.getAllIssues();
  }


  getAllIssues(){
    this.issueService.getIssues().subscribe(
      data => {
        console.log(data);
        this.issues = data;
      }
    )
  }

}
