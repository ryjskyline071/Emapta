import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChangeLog } from 'src/app/model/change-log';
import { Issue } from 'src/app/model/issue';
import { IssueService } from 'src/app/service/issue.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-update-issue',
  templateUrl: './update-issue.component.html',
  styleUrls: []
})
export class UpdateIssueComponent implements OnInit {

  issue: any;
  issues: Issue[] = [];
  id: number;
  usernameList: string[] = [];

  issue_description: string | undefined;
  issue_state: string | undefined;
  created_user: string | undefined;
  change_log: ChangeLog | undefined;
  prev_state: string | undefined;
  current_state: string | undefined;
  project_id: number | undefined;
  user_id: number | undefined;

  constructor(private route: ActivatedRoute, private issueService: IssueService, private userService: UserService) { 
    this.id = +this.route.snapshot.params['issueId'];
  }

  ngOnInit(): void {
    this.getIssue(this.id);
    this.getUsernameList();
  }

  getIssue(id: number) : any{
    this.issueService.getIssue(id).subscribe(
      data => {
         this.issue = data;
         this.prev_state = this.issue.current_state;
         this.project_id = this.issue.project_id;
         this.user_id = this.issue.user_id
         return this.issue;
      }
    )
  }

  getUsernameList(){
    this.userService.getUsernames().subscribe(
      data => {
         this.usernameList = data;
      }
    )
  }

  updateIssue(){
    if (confirm("Are you sure to update?")) {

      const change_log={
        to_state : this.issue.current_state,
        from_state : this.prev_state
      }

      const issue ={
        issue_description : this.issue.issue_description,
        type : this.issue.type,
        current_state: this.issue.current_state,
        created_user : this.issue.created_user,
        change_log: change_log,
        project_id: this.project_id,
        user_id: this.user_id
      }

      this.issueService.updateIssue(this.id, this.issue);
      
      // .subscribe(
      //   response => {
      //     console.log(issue);
      //     alert("Issue has been updated");
      //     window.location.href = './issues';
      //   },error => {
      //     console.log(error);
      //     alert("An error occured while adding the issue");
      //   }
      // )
    } 
  }
}

