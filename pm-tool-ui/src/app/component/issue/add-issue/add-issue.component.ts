import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Issue } from 'src/app/model/issue';
import { IssueService } from 'src/app/service/issue.service';

@Component({
  selector: 'app-add-issue',
  templateUrl: './add-issue.component.html',
  styleUrls: []
})
export class AddIssueComponent implements OnInit {

  issue: Issue = {
    issue_id : undefined,
    issue_description: undefined,
    type : undefined,
    current_state: undefined,
    created_user: undefined,
    created_date: undefined,
    change_log: undefined,
    project_id: undefined,
    user_id: undefined
  };

  constructor(private issueService: IssueService) { }

  ngOnInit(): void {
  }

  saveIssue(){
    const issue ={
      issue_description : this.issue.issue_description,
      type : this.issue.type,
      current_state: this.issue.current_state,
      created_user : this.issue.created_user,
      project_id : this.issue.project_id
    }

    this.issueService.saveIssue(issue).subscribe(
      response => {
        alert("Issue has been created");
        window.location.href = './issues';
      },error => {
        console.log(error);
        alert("An error occured while adding the issue");
      }
    )
  }

}
