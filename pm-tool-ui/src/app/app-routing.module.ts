import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddProjectComponent } from './component/project/add-project/add-project.component';
import { HomeComponent } from './component/home/home.component';
import { IssueComponent } from './component/issue/view-issue/issue.component';
import { ProjectIssuesComponent } from './component/project/project-issues/project-issues.component';
import { ProjectComponent } from './component/project/view-project/project.component';
import { AddIssueComponent } from './component/issue/add-issue/add-issue.component';
import { UpdateIssueComponent } from './component/issue/update-issue/update-issue.component';
import { ViewUserComponent } from './component/user/view-user/view-user.component';
import { AddUserComponent } from './component/user/add-user/add-user.component';
import { LoginComponent } from './component/user/login/login.component';

const routes: Routes = [
  {
    path: 'home',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'projects',
    pathMatch: 'full',
    component: ProjectComponent,
  },
  {
    path: 'issues',
    pathMatch: 'full',
    component: IssueComponent
  },
  {
    path: 'users',
    pathMatch: 'full',
    component: ViewUserComponent
  },
  {
    path: 'users/add',
    pathMatch: 'full',
    component: AddUserComponent
  },
  {
    path: 'view/issues/:projectId',
    pathMatch: 'full',
    component: ProjectIssuesComponent
  },
  {
    path: 'projects/add',
    pathMatch: 'full',
    component: AddProjectComponent
  },
  {
    path: 'issues/add',
    pathMatch: 'full',
    component: AddIssueComponent
  },
  {
    path: 'update/issue/:issueId',
    pathMatch: 'full',
    component: UpdateIssueComponent
  },
  {
    path: 'login',
    pathMatch: 'full',
    component: LoginComponent
  }
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
